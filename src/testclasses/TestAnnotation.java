package testclasses;

import org.testng.annotations.Test;
import appcode.ClassToTest;

public class TestAnnotation {

	@Test(priority=1)
	public void testMethod1() {
		ClassToTest obj = new ClassToTest();
		int result = obj.sumNumber(3, 112);
		System.out.println("Running test => testMethod1 result: "+result);
	}

	@Test(priority=0)
	public void testMethod2() {
		System.out.println("Running test => testMethod2");
	}

	@Test(priority=2)
	public void testMethod3() {
		System.out.println("Running test => testMethod3");
	}

}
