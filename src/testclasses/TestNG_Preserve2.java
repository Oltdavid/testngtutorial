package testclasses;

import org.testng.annotations.Test;

public class TestNG_Preserve2 {
	@Test
	public void TestMethod1() {
		System.out.println("TestNG_Preserve2 -> TestMethod1");
	}
	@Test
	public void TestMethod2() {
		System.out.println("TestNG_Preserve2 -> TestMethod2");
	}
}
