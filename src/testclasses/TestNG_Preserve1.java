package testclasses;

import org.testng.annotations.Test;

public class TestNG_Preserve1 {
	@Test
	public void TestMethod1() {
		System.out.println("TestNG_Preserve1 -> TestMethod1");
	}
	@Test
	public void TestMethod2() {
		System.out.println("TestNG_Preserve1 -> TestMethod2");
	}
}
